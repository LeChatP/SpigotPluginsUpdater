# Spigot Plugin Updater

this script permit to update plugins on Spigot website (also paid)
this script uses cfscrape to bypass anti-bot protection
This script is used on multiserver folder, just specify in the variable server the location.

## REQUIREMENTS

Python, Requests, Cfscrape, Linux/Unix, Node.js
Spigot Account (and bought concerning premium ressources)

## Setup REQUIREMENTS

sudo apt install python python-pip&&pip install pipenv&&pipenv install requests cfscrape

## Usage

python updatePlugins.py [server/all] <plugin>...

## TODO

make a config file to adapt script for all

## Licence
this project is protected by GNU GENERAL PUBLIC LICENSE v3
