import requests
import zipfile
import tqdm
import os
import shutils

def download(url,filename):
    zipdownload = requests.get(url, stream=True)
    lowerdict = {k.lower():v for k,v in zipdownload.headers.items()}
    try:
        size=float(lowerdict['content-length'])
        bar = tqdm(total=size, unit='iB')
        bar.update(0)
    except:
        bar = None
    with open(filename, 'wb') as f:
        for chunk in jar.iter_content(chunk_size=1024):
            if bar is not None:
                bar.update(len(data))
            f.write(chunk)
        f.close()
    if bar is not None:
        bar.close()

def unzip(filename):
    z = zipfile.ZipFile(filename, 'r')
    z.extractall()

def rmrecurse(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

if __name__ == "__main__":
    releases = requests.get("https://github.com/FlareSolverr/FlareSolverr/releases")
    search = re.search(r'<a href="(.*)(linux-x64.zip)" rel="nofollow" class="d-flex flex-items-center min-width-0">', info, re.IGNORECASE)
    url = "https://github.com"+search.group(1)+search.group(2)
    basedir = os.path.dirname(os.path.realpath(__file__))
    filename = os.path.join(basedir,"FlareSolverr.zip")
    dest = os.path.join(basedir,"flaresolverr/")
    download(url,filename)
    rmrecurse(dest)
    unzip(filename)
