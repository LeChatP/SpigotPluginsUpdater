import os
from threading import Thread
from subprocess import Popen, PIPE
import logging
import requests
import base64
import tqdm
import time


class FlareSolverr(Thread):
    def __init__(self, binary, url = "127.0.0.1", port = 8191, logfile = "flaresolverr.log", group=None, target=None, name=None, session="SpigotPluginsUpdater"):
        super(FlareSolverr,self).__init__(group=group, target=target, name=name)
        self._url = url
        self._port = port
        self._binary = binary
        self._logfile = os.path.join(os.path.dirname(os.path.realpath(__file__)),logfile)
        self._session = session
        self._userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36"
        self.__createSession()
    
    def __del__(self):
        self.__destroySession()

    def __startServer(self):
        os.environ["HOST"] = self._url
        os.environ["PORT"] = str(self._port)
        with Popen([self._binary], stdout=PIPE) as proc, open(self._logfile,"w") as log:
            try:
                log.write(proc.stdout.read().decode())
            finally:
                proc.kill()
                log.close()

    def __getFlareUrl(self):
        return "http://{}:{}/v1".format(self._url,self._port)
    
    def __askFlare(self,pjson):
        i = 1
        maxrange = 3
        pjson['session'] = self._session
        pjson['userAgent'] = self._userAgent
        for i in range(1,maxrange):
            try:
                print(str(pjson))
                response = requests.post(self.__getFlareUrl(),json=pjson)
                jsonres = response.json()
                if jsonres['status'] != "ok":
                    time = jsonres['endTimestamp'] - jsonres['startTimestamp']
                    print(" {:.2f}s : {}".format(float(time/1000),jsonres['message']))
                    if i < maxrange:
                        print("Attempt #{}".format(i+1))
                    else:
                        print("Unable to connect, skip")
                else:
                    break
            except Exception as e:
                print(e)
                continue
        return response

    def run(self):
        self.__startServer()
        return
    
    def __createSession(self):
        return self.__askFlare({
                "cmd": "sessions.create"
        }).status_code
    
    def __destroySession(self):
        return self.__askFlare({
                "cmd": "sessions.destroy"
        }).status_code
    
    def get(self, url, headers = {}, download = False, cookies={}, filename=None, **kwargs):
        if download:
            jar = requests.get(url, cookies=cookies, stream=True)
            start = time.clock()
            lowerdict = {k.lower():v for k,v in jar.headers.items()}
            size = float(lowerdict['content-length'])
            bar = tqdm.tqdm(total=size, unit='iB')
            bar.update(0)
            with open(filename, 'wb') as f:
                for chunk in jar.iter_content(chunk_size=1024):
                    if bar is not None:
                        bar.update(len(chunk))
                    f.write(chunk)
                f.close()
            if bar is not None:
                bar.close()
            print(" download:{:.2f}s".format((time.clock() - start)))
        return self.__askFlare({
                "cmd": "request.get",
                "url": url,
                "maxTimeout": 60000,
                "headers": headers,
                "download": download
        }).json()

    def dict_to_urlencoded_text(self,dict_item : dict):
        return str(dict_item)[1:-1].replace(", ","&").replace(": ","=").replace("'","")
    
    def post(self, url, headers = {'Content-Type':'text/html'}, data= {}, **kwargs):
        return self.__askFlare({
                "cmd": "request.post",
                "url": url,
                "maxTimeout": 6000,
                "headers": headers,
                "postData": self.dict_to_urlencoded_text(data)
        }).json()

