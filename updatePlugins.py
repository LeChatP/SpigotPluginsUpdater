#!/usr/bin/python3
#-*- coding: utf-8 -*-
# /!\ REQUIRE requests, 

import sys, os, platform
import json
import cloudscraper
import requests, pickle
import configparser
import re
import time
import ast
import zipfile
import base64
from lxml import html
from tqdm import tqdm
import argparse
from scraper import FlareSolverr

dirname=os.path.dirname

# CLASS FOR KAWAY COLORS
class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
        def disable(self):
            self.HEADER=''
            self.OKBLUE=''
            self.OKGREEN=''
            self.WARNING=''
            self.FAIL=''
            self.ENDC=''
            self.BOLD=''
            self.UNDERLINE = ''
bcolors = bcolors()
# /!\ REQUIRE Unix
if platform.system() == "Windows":
    print(bcolors.FAIL+"Ce script ne fonctionne que sur Unix")
    sys.exit(1)

config = {}
basedir = dirname(os.path.abspath(__file__))
with open(basedir+"/config.json","r") as conf:
    config = json.load(conf)
serverdir = config['serverdir']
cookiejar = config['cookiejar']
username = config['username']
data = config['data']


plugins = []
scraper = FlareSolverr(basedir+"/flaresolverr/flaresolverr")


def scraperWorks():
    try:
        print("Does scraper works?")
        response = scraper.get("https://www.spigotmc.org")
        time = response['endTimestamp'] - response['startTimestamp']
        sys.stdout.write("scraperWorks:{:.2f}s".format(float(time/1000)))
        assert response['status'] == "ok"
        print(" IT WORKS :)")
        return True
    except Exception as e:
        print(" No :(")
        return False
works = False

# GET CONFIG FILE OF PLUGIN
def getConfig(filename):
    config = configparser.ConfigParser()
    with open(filename,'r') as f:
        content = f.read()
        if "[Plugin]" not in content.split('\n')[0]:
            content = "[Plugin]\n" + content
    #print(content)
    config.read_string(content)
    return config

# SAVE CONFIG FILE OF PLUGIN
def saveConfig(config : configparser.ConfigParser,filename):
    cfgfile = open(filename,'w')
    config.write(cfgfile)
    cfgfile.close()



def isLogin(cookies):
    try:
        response = scraper.get("https://www.spigotmc.org",cookies=cookies)
        time = response['endTimestamp'] - response['startTimestamp']
        print("isLogin:{:.2f}s".format(float(time/1000)))
        return username in response['solution']['response']
    except:
        print("Scraper failed")
        return False

# LOGIN HANDLING
def getCookies() -> dict:

    def getCookiesFromScraper(response):
        cookies = {}
        for cookie in response['solution']['cookies']:
            cookies[cookie['name']]=cookie['value']
        return cookies
    
    def saveCookies(cookies):
        global cookiejar
        with open(cookiejar,'w') as f:
            if cookies is not dict:
                cookiejar = requests.utils.dict_from_cookiejar(cookies)
            else:
                cookiejar = cookies
            pickle.dump(cookiejar,f,pickle.HIGHEST_PROTOCOL)

    def loadCookies() ->dict:
        with open(cookiejar,'r') as f:
            return pickle.load(f)

    #return new cookies
    # def makeCookies() -> dict:
    #     try:
    #         response = scraper.get("https://www.spigotmc.org")
    #         time = response['endTimestamp'] - response['startTimestamp']
    #         sys.stdout.write(" makeCookies:{:.2f}s".format(float(time/1000)))
    #         return getCookiesFromScraper(response)
    #     except:
    #         return requests.utils.dict_from_cookiejar(requests.get("https://github.com").cookies)
    
    #return logged cookies
    def doLogin():
        response = scraper.post('https://www.spigotmc.org/login/login', header={"Content-type":"text/html"} ,data=data)
        time = response['endTimestamp'] - response['startTimestamp']
        print("doLogin:{:.2f}s".format(float(time/1000)))
        return getCookiesFromScraper(response)
    if not os.path.isfile(cookiejar):
        cookies = {}
    else:
        cookies = loadCookies()
    if not isLogin(cookies):
        cookies = doLogin()
        if not isLogin(cookies):
            raise requests.ConnectionError(" Unable to login")
    saveCookies(cookies)
    return cookies

def getSpigot(info):
    try:
        version = re.search(r'download\?version=([0-9]+)', info, re.IGNORECASE).group(1)
        extension = re.search(r'[0-9.]+ [KM]{1}B ([a-zA-Z0-9.]+)', info, re.IGNORECASE).group(1)
        return version,extension,None
    except:
        raise requests.ConnectionError(" Unable to login, you tried too many times")

def getGithub(info,fileregex):
    try:
        version = re.search(r'<code>([0-9a-fA-F]*)</code>', info, re.IGNORECASE).group(1)
        if fileregex is not None:
            search = re.search(r'<a href="(.*'+fileregex+')" rel="nofollow" class="d-flex flex-items-center min-width-0">', info, re.IGNORECASE)
            extension = ".jar"
            url = "https://github.com"+search.group(1)
        else:
            search = re.search(r'<a href="(.*)(.[a-zA-Z]*)" rel="nofollow" class="d-flex flex-items-center min-width-0">', info, re.IGNORECASE)
            url = "https://github.com"+search.group(1)+search.group(2)
            extension = search.group(2)
        return version,extension,url
    except Exception as e:
        print(e)
        raise e

def getJenkins(url,info,fileregex):
    try:
        version = re.search(r'<wbr>#(.*)</h1>', info, re.IGNORECASE).group(1)
        if fileregex is not None:
            search = re.search(r'>('+fileregex+')<', info, re.IGNORECASE)
            extension = ".jar"
            url = url+search.group(1)
        else:
            search = re.search(r'>([^>]*).jar<', info, re.IGNORECASE)
            url = url+search.group(1)+".jar"
            extension = ".jar"
        return version,extension,url
    except Exception as e:
        print(e)
        raise e

def getBukkit(info):
    tree = html.fromstring(info)
    version = tree.xpath("//abbr[@class='tip standard-date standard-datetime']/@data-epoch")
    
    extension = ".jar"
    url = "https://dev.bukkit.org"+re.search(r'<a class="button alt fa-icon-download" href="(.*)">', info, re.IGNORECASE).group(1)
    return version,extension,url

def getEngineHub(info):
    pass

# GETTING DOWNLOAD INFORMATIONS
def getInformations(works,url,fileregex):
    cookies = None
    if works and ("www.spigotmc.org" in url or "dev.bukkit.org" in url):
        cookies = getCookies()
        response = scraper.get(url, cookies=cookies)
        time = response['endTimestamp'] - response['startTimestamp']
        sys.stdout.write(" getInfo:{:.2f}s ".format(float(time/1000)))
        if "www.spigotmc.org" in url:
            return getSpigot(response['solution']['response'])
        elif "dev.bukkit.org" in url:
            return getBukkit(response['solution']['response'])
    if "github.com" in url:
        return getGithub(requests.get(url).content.decode(),fileregex)
    elif "www.spigotmc.org" not in url and "dev.bukkit.org" not in url:
        content = requests.get(url).content.decode()
        if "[Jenkins]" in content:
            return getJenkins(url,content,fileregex)
        raise Exception(" Unrecognized URL, Download manually : {}".format(url))
    else:
        raise Exception(" Scraper does not works Download manually : {}".format(url))

# DOWNLOAD FILE WITH KAWAY PROGRESS BAR
def download(url,version,filename):
    if "www.spigotmc.org" in url or "dev.bukkit.org" in url:
        cookies = getCookies()
        #cookies = None
        if "www.spigotmc.org" in url:
            extension = "download?version=" + version
        else:
            extension = ""
        response = scraper.get(url + extension, cookies=cookies, download=True, filename=filename)
    else:
        jar = requests.get(url, stream=True)
        lowerdict = {k.lower():v for k,v in jar.headers.items()}
        try:
            size=float(lowerdict['content-length'])
            bar = tqdm(total=size, unit='iB')
            bar.update(0)
        except:
            bar = None
        with open(filename, 'wb') as f:
            for chunk in jar.iter_content(chunk_size=1024):
                if bar is not None:
                    bar.update(len(data))
                f.write(chunk)
            f.close()
        if bar is not None:
            bar.close()
# DELETE OLD FILES > 1 MONTH
def garbageOld(folder):
    for ofile in os.listdir(folder):
        if ofile.endswith('.old'):
            if time.time() - os.path.getmtime(folder+'/'+ofile) > (30 * 24 * 60 * 60):
                os.remove(folder+'/'+ofile)

# CHECK PLUGIN UPDATE
def checkFileUpdate(works,filename):
    sys.stdout.write(bcolors.OKBLUE+"Plugin "+os.path.basename(filename)[:-9]+bcolors.ENDC) 
    conf = getConfig(filename)
    url = conf.get('Plugin', 'url')
    try:
        newversion,extension,newurl = getInformations(works,url,conf.get('Plugin','fileregex',fallback=None))
        if newurl is not None:
            url = newurl
    except requests.ConnectionError as e: 
        print(e)
        return
    except Exception as e:
        print(e)
        return
    try:
        version = conf.get('Plugin','version')
    except:
        conf.set('Plugin','version',str(newversion))
        version = 0
    if version != newversion:
        sys.stdout.write(bcolors.OKGREEN+" New version is available ! Downloading..."+bcolors.ENDC)
        if extension not in filename:
            if  extension not in ['.zip']:
                print(" Cannot download new version because file isn't a jar or a compatible file")
                return
            if not conf.has_option('Plugin','fileregex'):
                print(" to decompress zip file, config must have a fileregex variable who can select correct file")
                print("it use regex to get file (more informations here https://en.wikipedia.org/wiki/Regular_expression")
                print("like this : ")
                print("fileregex = EssentialsX-[0-9]+.jar")
                return
            if os.path.isfile(filename[:-5]):
                os.rename(filename[:-5],filename[:-5]+'.'+str(version)+'.old')
            download(url,newversion,filename[:-5]+extension)
            with open(filename[:-5]+extension,'r') as f:
                if extension == '.zip':
                    z = zipfile.ZipFile(f, 'r')
                    matched = False
                    dirname = os.path.dirname(filename)
                    for info in z.infolist():
                        if re.match(conf.get('Plugin','fileregex'),info.filename):
                            z.extract(info,dirname)
                            os.rename(dirname+'/'+info.filename,filename[:-5])
                            matched = True
                            break
                    if not matched:
                        print('File not matched! Not Updated!')
                    z.close()
                f.close()
            os.remove(filename[:-5]+extension)
        else:
            if os.path.isfile(filename[:-5]):
                os.rename(filename[:-5],filename[:-5]+'.'+str(version)+'.old')
            download(url,newversion,filename[:-5])

        conf.set('Plugin','version',str(newversion))
    else:
        sys.stdout.write(bcolors.OKBLUE+" No new version\n"+bcolors.ENDC)
    saveConfig(conf,filename)

def getMotd(folder):
    try:
        with open(folder+'/server.name','r') as props:
            return props.read()
    except:
        return folder.split('/')[-1]

def getServer(serverdir,name):
    try:
        for filename in os.listdir(serverdir):
            directory = serverdir + filename + "/server.name"
            if os.path.exists(directory):
                with open(serverdir + filename +'/server.name','r') as props:
                    if name == props.read().replace('\n',''):
                        return filename
    except:
        pass
    return None

# CHECK SPECIFIC PLUGINS IN SERVER
def checkFilesFolderUpdate(works,folder,names): 
    sys.stdout.write(bcolors.HEADER) 
    motd = getMotd(dirname(folder))
    print((" SERVER '''"+motd+"''' ").center(80,'-'))
    sys.stdout.write(bcolors.ENDC)
    for filename in os.listdir(folder):
        if filename in names:   
            checkFileUpdate(works,folder+'/'+filename)
    garbageOld(folder)

# CHECK ALL PLUGINS IN SERVER
def checkFolderUpdate(works,folder):
    sys.stdout.write(bcolors.HEADER)
    motd = getMotd(dirname(folder))
    print((" SERVER '''"+motd+"''' ").center(80,'-'))
    sys.stdout.write(bcolors.ENDC)
    for filename in os.listdir(folder):
        if filename.endswith('.jar.conf'): # and os.path.isfile(folder + '/' + filename[:-5]):
            checkFileUpdate(works,folder+'/'+filename)
    garbageOld(folder)

# CHECK ALL PLUGINS UN ALL SERVERS
def checkServers(works, serverdir,plugins=[]):
    for folder in os.listdir(serverdir):
        directory = serverdir + folder + "/plugins"
        if os.path.exists(directory): 
            if not plugins:
                checkFolderUpdate(works,directory)
            else:
                checkFilesFolderUpdate(directory, plugins)

def parse():
    parser = argparse.ArgumentParser(prog="!minecraft update", formatter_class=argparse.RawDescriptionHelpFormatter,
    description="This script updates Bukkit/Spigot plugins (also premium). As you can know, these websites are protected by CloudFlare anti-bot. But this script is equiped with a engine that can bypass this protection. But sometimes the protection mecanism changes and engine is unavailable. So sometimes you need to manually update these plugins.\n\
That's also why This script can handle Github and Jenkins download websites, which are not protected. So if you can found your plugin in these website it would be better.\n\n\
#### Setup ####\n\n\
Create 'yourplugin.jar.conf' file\n\n\
## [For Spigot/Bukkit] ##\n\n\
Here is how the created file works for Spigot and Bukkit website. At the first line you can specify the url of resource.\n\n\
url=http://www.spigotmc.org/resources/plugin.12345/\n\n\
The URL must be the mainpage of resource\
There's also optionnal line that you can add :\n\n\
`fileregex=EssentialsX-[0-9]+.jar`\n\n\
The fileregex is for .zip downloaded file plugin, use regexp to grab the correct file in the archive\n\n\
**## [For Github] ##**\n\n\
First you need to be sure that Github project of plugin provides the jar file. You need to check if there's a Release webpage and there's jar file in releases. Note : Source code DOES NOT WORK, only jar File.\nExample which works : https://github.com/elian1203/ezAuctions/releases. The 1.6.2 Version provides .jar file\nExample which does not works: https://github.com/lucko/LuckPerms/releases -> The release provides only zip and tar.gz that are source code, no JAR file.\n\
So url must be like this : url=https://github.com/Developer/Plugin/releases\n\
If not, that will not work.\n\n\
**## [For Jenkins] ##**\n\n\
Jenkins repo are uncommon. As I know only LuckPerms is using for public.\n\
It needs a url like this : https://ci.lucko.me/job/LuckPerms/lastSuccessfulBuild/artifact/bukkit/loader/build/libs/\n\
It's URL which lead to (lastSucessfulBuild) JAR file download link. (Click on link to see what it should looks like)\n\n\
**## [For Github/Jenkins] ##**\n\n\
If Release webpage or Jenkins webpage provides multiple JAR file. You can specify to select only the JAR that you need. By default, it will be the first jar file.\n\
To Select only specific jarfile use : `fileregex=EssentialsX-[0-9]+.jar` This accepts REGEX but not grouping regex (use 'not grouping' group instead).\n\n\
**# Informations : **\n\n\
Server Directory : {SERVERDIR}\n\
Login : {LOGIN}\n\
Password : {PASSWORD}\n\
Cookie-Jar : {COOKIE}\n".format(SERVERDIR=serverdir,
            LOGIN=str(data['login']),
            PASSWORD="*" * len(data['password']),
            COOKIE=cookiejar))
    parser.add_argument("server", help="ALL or FolderName or ServerName specified in server.name file")
    parser.add_argument("plugins", nargs='*', default=[], help="Plugins to update specifically, if not, all plugins will be updated")
    args = parser.parse_args()
    return args


# BEGIN PROGRAM
if __name__ == "__main__":
    args = parse()
    works = scraperWorks()
    plugins = [s + ".jar.conf" for s in args.plugins]
    server = args.server
    if args.server.lower() == "all":
        checkServers(works,serverdir,plugins)
        sys.exit(0)
    elif os.path.exists(serverdir + server + '/plugins'):
        path = serverdir + server + '/plugins'
    else:
        directory = getServer(server)
        if directory is not None:
            path = serverdir + directory + '/plugins'
        else:
            print("This Server name does not exist, check local parent folder or server.name file of target server")
            sys.exit(1)
    if plugins:
        checkFilesFolderUpdate(works,path,plugins)
    else:
        checkFolderUpdate(works,serverdir + server + '/plugins')
